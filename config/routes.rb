Rails.application.routes.draw do
  # Root route
  root 'home#home'

  # Default resource routes
  resources :room_messages
  resources :rooms
  resources :goals
  resources :room_messages
  resources :rooms
  resources :leaderboards

  # Custom + default routes for habits
  resources :habits do
    post :track, on: :member
    post :reset_streak, on: :member
    post :reset_max_streak, on: :member
    resources :schedules
  end


  # Custom routes for goals
  get 'completed', to: 'goals#completed'
  get 'createBaseGoals', to: 'goals#createBaseGoals'
  get 'createHabitGoals', to: 'goals#createHabitGoals'
  get 'goals', to: 'goals#index'
  get 'updateGoals', to: 'goals#updateGoals'

  # Devise routes for user auth
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  # Routes for legal stuff
  get 'policy/privacy' => 'legal#privacy'
  get 'policy/security'=> 'legal#security'
  get 'policy/cookie-notice'=> 'legal#cookieInfo'
  post '/push'=> 'home#push'

  # API routes that are required for react to work
  namespace :api do
    namespace :v1 do
      get 'habits/index' => 'habits#index'
      get 'habits/show' => 'habits#show'
      post 'habits/push_notification' => 'habits#push_notification'
      post 'habits/add_push_endpoint' => 'habits#add_push_endpoint'
    end
  end

  # Custom routes for leaderboards
  post 'joinWcode', to: 'leaderboards#join'

  # Custom routes for leaderboard invites
  post 'leaveBoard', to: 'invites#leave'
  post 'inviteUser', to: 'invites#invite'
  post 'acceptInvite', to: 'invites#accept'
  post 'rejectInvite', to: 'invites#reject'


end
