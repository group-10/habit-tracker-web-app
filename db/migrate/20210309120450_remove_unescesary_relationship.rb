class RemoveUnescesaryRelationship < ActiveRecord::Migration[5.2]
  def change
    remove_column :tracking_entries, :user_id
  end
end
