class AddMorecolumnToGoals < ActiveRecord::Migration[5.2]
  def change
    add_column :goals, :type, :string
    add_column :goals, :completed, :boolean
  end
end
