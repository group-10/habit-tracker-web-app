class AddHabitFields < ActiveRecord::Migration[5.2]
  def change
    add_column :habits, :streak, :int
    add_column :habits, :max_streak, :int
    add_column :habits, :target, :int
  end
end
