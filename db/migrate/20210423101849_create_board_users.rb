class CreateBoardUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :board_users do |t|
      t.string :status
      t.belongs_to :user, foreign_key: true
      t.belongs_to :leaderboard, foreign_key: true

      t.timestamps
    end
  end
end
