class CreateLeaderboards < ActiveRecord::Migration[5.2]
  def change
    create_table :leaderboards do |t|
      t.string :title, null: false

      t.timestamps
    end
  end

  def self.down
    drop_table :impressions
  end
end
