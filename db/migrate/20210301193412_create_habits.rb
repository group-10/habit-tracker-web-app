class CreateHabits < ActiveRecord::Migration[5.2]
  def change
    create_table :habits do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :impressions
  end
end
