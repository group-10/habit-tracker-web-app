class AddTrackDateToHabit < ActiveRecord::Migration[5.2]
  def change
    add_column :habits, :last_tracked, :datetime
  end
end
