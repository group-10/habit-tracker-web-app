class AddLeaderboardFields < ActiveRecord::Migration[5.2]
  def change
    add_column :leaderboards, :description, :text
    add_column :leaderboards, :privacy, :boolean
  end
end
