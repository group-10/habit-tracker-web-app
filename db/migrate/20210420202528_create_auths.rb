class CreateAuths < ActiveRecord::Migration[5.2]
  def change
    create_table :auths do |t|
      t.string :uid
      t.string :provider
      t.belongs_to :user, foreign_key: true
      t.timestamps
      t.index ["provider", "uid"], name: "index_authorizations_on_provider_and_uid"
    end
  end
end
