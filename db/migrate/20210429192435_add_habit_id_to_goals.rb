class AddHabitIdToGoals < ActiveRecord::Migration[5.2]
  def change
      add_column :goals, :habit_id, :integer
      add_foreign_key :goals, :habits, column: :habit_id
      add_foreign_key :goals, :users, column: :user_id
  end
end
