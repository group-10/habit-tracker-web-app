class ChangeScheduleDataTypes < ActiveRecord::Migration[5.2]
  def change
    change_column :schedules, :starts_at, :time
    change_column :schedules, :ends_at, :time
    add_column :schedules, :date, :date
  end
end
