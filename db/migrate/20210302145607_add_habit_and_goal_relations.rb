class AddHabitAndGoalRelations < ActiveRecord::Migration[5.2]
  def change
    add_reference :goals, :user, index: true
    add_reference :habits, :user, index: true
  end
end
