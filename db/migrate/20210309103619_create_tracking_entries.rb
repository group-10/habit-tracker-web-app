class CreateTrackingEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :tracking_entries do |t|
      t.datetime :start
      t.datetime :end
      t.belongs_to :habit, foreign_key: true
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
