class RenameEntryFields < ActiveRecord::Migration[5.2]
  def change
    rename_column :tracking_entries, :start, :trackStart
    rename_column :tracking_entries, :end, :trackEnd
  end
end
