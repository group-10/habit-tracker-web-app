class RenameTypeField < ActiveRecord::Migration[5.2]
  def change
    rename_column :goals, :type, :goaltype
  end
end
