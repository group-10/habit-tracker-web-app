class RenameScheduleVariables < ActiveRecord::Migration[5.2]
  def change
      rename_column :schedules, :start, :starts_at
      rename_column :schedules, :end, :ends_at
  end
end
