class CreateUserLeaderboardJoinTable < ActiveRecord::Migration[5.2]
  def self.up
  # Model names in alphabetical order (e.g. a_b)
    create_table :users_leaderboards, :id => false do |t|
      t.integer :leaderboard_id
      t.integer :user_id
    end

    add_index :users_leaderboards, [:leaderboard_id, :user_id]
  end

  def self.down
    drop_table :users_leaderboards
  end
end
