class AddRoomsToLeaderboards < ActiveRecord::Migration[5.2]
  def change
    add_reference :leaderboards, :room, index: true
  end
end
