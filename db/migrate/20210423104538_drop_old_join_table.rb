class DropOldJoinTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :users_leaderboards
  end
end
