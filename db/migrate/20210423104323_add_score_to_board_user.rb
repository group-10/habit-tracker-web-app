class AddScoreToBoardUser < ActiveRecord::Migration[5.2]
  def change
    add_column :board_users, :score, :integer
  end
end
