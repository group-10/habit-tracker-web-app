class CreateGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :goals do |t|
      t.string :title, null: false
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :impressions
  end
end
