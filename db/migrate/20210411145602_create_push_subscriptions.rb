class CreatePushSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :push_subscriptions do |t|
      t.string :endpoint, null: false
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :impressions
  end
end
