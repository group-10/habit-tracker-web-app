class AddFieldsToPushSubscription < ActiveRecord::Migration[5.2]
  def change
    add_column :push_subscriptions, :auth, :string
    add_column :push_subscriptions, :p256dh, :string
  end
end
