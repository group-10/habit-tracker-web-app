# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.destroy_all
Leaderboard.destroy_all

BobRoss=User.create(email: "BobRoss@art.com", password: "HappyLittleAccidents", name: "Bob Ross")
Steve = User.create(email: "steve@steve.com", password: "123456", name: "Steve")
BobRoss.save!
Steve.save!
BobRoss.habits.create(user:BobRoss, title: "Do some painting", description:"Painty Painty paint", streak:20, target:60,
                      last_tracked: DateTime.new(2021,3,28, 6))
BobRoss.habits.create(user:BobRoss, title: "Test it", description: "Test testy test", streak:10, target: 60,
                      last_tracked: DateTime.new(2021,3,28, 6))
Steve.habits.create(user:Steve, title: "Test it", description: "Test testy test", streak:41, target: 60,
                    last_tracked: DateTime.new(2021,3,28, 6))