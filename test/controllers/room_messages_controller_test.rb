require 'test_helper'

class RoomMessagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include HabitsHelper
  setup do
    sign_in users(:one)
    @leaderboard = leaderboards(:one)
    @room = rooms(:one)
    @message = room_messages(:one)
  end
  test 'should create message' do
    assert_difference("RoomMessage.count") do
      post "/room_messages", params: {room_message: {user_id: 1, room_id: 1, message:"Default Message"}}
      end
      assert_response :success
  end

end
