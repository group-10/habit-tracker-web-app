require 'test_helper'

class RoomsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include HabitsHelper
  setup do
    sign_in users(:one)
    @leaderboard = leaderboards(:one)
    @room = rooms(:one)
  end
  test "should show rooms index" do
    get rooms_url
    assert_response :success
  end
  test "should show room" do
    get room_url(@room)
    assert_response :success
  end
  test "should create room" do
    get "/rooms/new"
    assert_response :success
    assert_difference( "Room.count") do
      post "/rooms", params: { room:
        {name: "New Room", id: 3}}
    end
    assert_redirected_to rooms_path
  end

end
