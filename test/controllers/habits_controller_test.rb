require 'test_helper'
require "habits_helper"

class HabitsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include HabitsHelper

  setup do
    sign_in users(:one)
    # post user_session_url, params: { session: { email: users(:one).email, encrypted_password: users(:one).encrypted_password } }
    # assert_response :success

    @habit = habits(:one)

  end



  test "should get new" do
    get new_habit_url
    assert_response :success

    assert_template layout: 'application'
    assert_select 'div.container', "Back\n  New Habit"
  end

  test "should destroy Habit" do
    assert_difference('Habit.count', -1) do
      delete habit_url(@habit)
    end

    assert_redirected_to root_url
  end

  test "should track correctly" do
    @habit2 = habits(:two)
    post track_habit_path(@habit2)
    assert_redirected_to root_url
    assert_equal "Good job!", flash[:notice]

    post track_habit_path(@habit2)
    assert_redirected_to root_url
    assert_equal "You have already done that today. Come back tomorrow!", flash[:notice]
  end


  test "should reset streak" do
    @habit= habits(:reset)
    puts "\n\n #{@habit.last_tracked}"
    post track_habit_path(@habit)

    assert_redirected_to root_url
    follow_redirect!
    assert_equal "You've lost you streak. Too bad!", flash[:alert]
    #assert_equal(1, @habit.streak)
  end

  test "should reset streak 2" do #assert_select cannot find 'div.habit-streak'
  @habit = habits(:reset)
    puts "\n\n #{@habit.last_tracked}"
    get root_url
    assert_equal( 13, @habit.streak)
  #assert_select 'div.habit-streak', "13"

    @habit.streak = get_habit_streak(@habit)
    get root_url
    assert_equal( 1, @habit.streak)
  #assert_select 'div.habit-streak', "1"
  end
end
