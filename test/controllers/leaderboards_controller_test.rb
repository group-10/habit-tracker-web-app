require 'test_helper'

class LeaderboardsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include HabitsHelper
  setup do
    sign_in users(:one)
    @leaderboard = leaderboards(:one)
    @user = users(:one)
    @leaderboard2 = leaderboards(:two)
  end

  test "should get index" do
    get leaderboards_url
    assert_response :success
  end

  test "should get new" do
    get new_leaderboard_url
    assert_response :success
  end

  test "should create leaderboard" do
    get "/leaderboards/new"
    assert_response :success
    assert_difference("Leaderboard.count") do
      post "/leaderboards", params: { leaderboard: { title: "Title 1", description: "Leaderboard description"}}
    end


  end

  test "should show leaderboard" do
    get leaderboard_url(@leaderboard)
    assert_response :success
  end

  test "should get edit" do
    get edit_leaderboard_url(@leaderboard)
    assert_response :success
  end

  test "should update leaderboard" do
    patch leaderboard_url(@leaderboard), params: { leaderboard: { title:"updated" } }
    assert_redirected_to leaderboard_url(@leaderboard)
    @leaderboard.reload
    assert_equal("updated", @leaderboard.title)
  end

  test "should destroy leaderboard" do
    assert_difference('Leaderboard.count', -1) do
      delete leaderboard_url(@leaderboard)
    end

    assert_redirected_to leaderboards_url
  end
  test "user should join" do
    post joinWcode_path(:code =>"2|Leaderboards Title 2|2")
    assert_equal "Leaderboard was successfully joined.", flash[:notice]
    assert_redirected_to leaderboards_path
  end
end
