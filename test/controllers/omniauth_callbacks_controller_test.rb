require 'test_helper'

class OmniauthCallbacksControllerTest < ActionDispatch::IntegrationTest
  setup do
     OmniAuth.config.test_mode = true
  end



  teardown do
    OmniAuth.config.test_mode = false
  end
end
