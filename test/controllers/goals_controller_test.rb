require 'test_helper'

class GoalsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include HabitsHelper
  setup do
    sign_in users(:one)
    @habit = habits(:one)
    @goal = goals(:one)
  end

  test "should get index" do
    get goals_url
    assert_response :success
  end
  test "should create BaseGoals" do
    assert_difference("Goals.count", difference = 2) do
      get createBaseGoals_path
    end
    assert_redirected_to goals_url
  end
  test "should create habit goals" do
    assert_difference("Goals.count", difference = 4) do
      get createHabitGoals_path
    end
    assert_redirected_to goals_url
  end
  test "should destroy goal" do
    assert_difference('Goals.count', -1) do
      delete goal_url(@goal)
    end
    assert_redirected_to goals_url
  end

end
