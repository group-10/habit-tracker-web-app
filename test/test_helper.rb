ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  set_fixture_class goals: Goals
  fixtures :all

  # include Devise::Test::IntegrationHelpers
  # include Warden::Test::Helpers
  #
  # def log_in(user)
  #   if integration_test?
  #     login_as(user, :scope => :user)
  #   else
  #     sign_in(user)
  #   end
  # end

  # Add more helper methods to be used by all tests here...
  # OmniAuth.config.test_mode = true
  # omniauth_hash_fb = { 'provider' => 'facebook',
  #                      'uid' => '12345',
  #                      'info' => {
  #                      'name' => 'test',
  #                      'email' => 'test@testsomething.com'
  #                    },
  #                       'extra' => {'raw_info' =>
  #                                 { 'location' => 'Chicago' }
  #                               }
  #                     }
  #
  # omniauth_hash_fail = { 'provider' => 'facebook',
  #                        'uid' => '12345',
  #                        'info' => {
  #                        },
  #                        'extra' => {'raw_info' =>
  #                                   { 'location' => 'Chicago'}
  #                                   }
  #                       }
  # OmniAuth.config.add_mock(:facebook, omniauth_hash_fb)
  # OmniAuth.config.add_mock(:facebook_fail, omniauth_hash_fail)

end
