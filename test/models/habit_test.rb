require 'test_helper'

class HabitTest < ActiveSupport::TestCase
  test 'should not save habit without title' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.streak = 0
    habit.target = 60

    habit.save
    refute habit.valid?
  end

  test 'should not save habit without user' do
    habit = Habit.new
    habit.description = "Test description"
    habit.title = "New Habit"
    habit.streak = 0
    habit.target = 60

    habit.save
    refute habit.valid?
  end

  test 'should not save habit with wrong title format' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.streak = 0
    habit.target = 60
    habit.title = "H@bit"

    habit.save
    refute habit.valid?
  end

  test 'should save valid habit' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.streak = 0
    habit.target = 60
    habit.title = "New Habit"

    habit.save
    assert habit.valid?
  end

  test 'should not save habit with too long or short title' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.streak = 0
    habit.target = 60
    habit.title = "A"

    habit.save
    refute habit.valid?

    habit.title = "AAAAAAAAAAAAAAAAAAAAA"
    habit.save
    refute habit.valid?
  end

  test 'should not save habit without target or target too low' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.streak = 0
    habit.title = "New Habit"

    habit.save
    refute habit.valid?

    habit.target = 3
    habit.save
    refute habit.valid?
  end

  test 'should not save habit without streak or streak too low' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.description = "Test description"
    habit.target = 60
    habit.title = "New Habit"

    habit.save
    refute habit.valid?

    habit.streak = -10
    habit.save
    refute habit.valid?
  end

  test 'should not save habit without description or with wrong format' do
    habit = Habit.new
    habit.user_id = users(:one).id
    habit.streak = 0
    habit.target = 60
    habit.title = "New Habit"

    habit.save
    refute habit.valid?

    habit.description = "H@bit"
    habit.save
    refute habit.valid?
  end
end
