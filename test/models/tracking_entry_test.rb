require 'test_helper'

class TrackingEntryTest < ActiveSupport::TestCase
  test "should not save without habit" do
    candidate = TrackingEntry.new(trackStart: tracking_entries(:one).trackStart,
                                  trackEnd: tracking_entries(:one).trackEnd)
    assert !candidate.save
  end
  test "should not save without start time" do
    candidate = TrackingEntry.new(habit:habits(:one), trackEnd: tracking_entries(:one).trackEnd)
    assert !candidate.save
  end
  test "should not save without end time" do
    candidate = TrackingEntry.new(habit:habits(:one), trackStart: tracking_entries(:one).trackStart)
    assert !candidate.save
  end
  test "should not allow time travel" do
    candidate = TrackingEntry.new(habit:habits(:one), trackStart: tracking_entries(:one).trackEnd,
                                  trackEnd: tracking_entries(:one).trackStart)
    assert !candidate.save
  end
  test "should not save zero duration records" do
    candidate = TrackingEntry.new(habit:habits(:one), trackStart: tracking_entries(:one).trackStart,
                                  trackEnd: tracking_entries(:one).trackStart)
    assert !candidate.save
  end
  test "should save valid entry" do
    candidate = TrackingEntry.new(habit:habits(:one), trackStart: tracking_entries(:one).trackStart,
                                  trackEnd: tracking_entries(:one).trackEnd)
    assert candidate.save
  end
end
