require 'test_helper'

class GoalTest < ActiveSupport::TestCase
  test 'should not save goal without title' do
    goal = Goals.new
    goal.user_id = users(:one).id
    goal.description = "Test description"
    goal.counter = 0

    goal.save
    refute goal.valid?
  end

  test 'should not save goal with wrong title format' do
    goal = Goals.new
    goal.user_id = users(:one).id
    goal.description = "Test description"
    goal.title = "Valid Go@l"
    goal.counter = 0

    goal.save
    refute goal.valid?
  end

  test 'should not save goal without user' do
    goal = Goals.new
    goal.description = "Test description"
    goal.title = "Valid Goal"
    goal.counter = 0

    goal.save
    refute goal.valid?
  end

  test 'should save valid goal' do
    goal = Goals.new
    goal.user_id = 1
    goal.description = "Test description"
    goal.title = "Valid Goal"
    goal.counter = 0
    goal.target = 5
    goal.goaltype = "habitAmount"
    goal.save
    assert goal.valid?
  end

  test 'should not save goal with too long or short title' do
    goal = Goals.new
    goal.user_id = users(:one).id
    goal.description = "Test description"
    goal.title = "A"
    goal.counter = 0

    goal.save
    refute goal.valid?

    goal.title = "AAAAAAAAAAAAAAAAAAAAA"
    goal.save
    refute goal.valid?
  end

  test 'should not save goal without counter or with counter too low' do
    goal = Goals.new
    goal.user_id = users(:one).id
    goal.description = "Test description"
    goal.title = "Test title"

    goal.save
    refute goal.valid?

    goal.counter = -1
    goal.save
    refute goal.valid?
  end

end
