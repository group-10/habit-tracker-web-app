require 'test_helper'

class LeaderboardTest < ActiveSupport::TestCase
  test 'should save valid leaderboard' do
    lb = Leaderboard.new
    lb.title = "Valid title"
    lb.description = "Valid description"

    lb.save
    assert lb.valid?
  end

  test 'should not save lb without description' do
    lb = Leaderboard.new
    lb.title = "Valid title"

    lb.save
    refute lb.valid?
  end

  test 'should not save lb without title' do
    lb = Leaderboard.new
    lb.description = "Valid description"

    lb.save
    refute lb.valid?
  end

  test 'should not save lb with title too long or too short' do
    lb = Leaderboard.new
    lb.description = "Valid description"
    lb.title = "A"

    lb.save
    refute lb.valid?

    lb.title = "AAAAAAAAAAAAAAAAAAAAA"
    lb.save
    refute lb.valid?
  end

  test 'should not save lb with title in wrong format' do
    lb = Leaderboard.new
    lb.description = "Valid description"
    lb.title = "Le@ader"

    lb.save
    refute lb.valid?
  end

  test 'should not save lb with description in wrong format' do
    lb = Leaderboard.new
    lb.description = "Inv@lid description"
    lb.title = "Valid title"

    lb.save
    refute lb.valid?
  end
end
