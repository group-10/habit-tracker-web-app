require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should create user' do
    user = User.new
    user.email = "test@test.com"
    user.password = "1234qwerty"

    user.save
    assert user.valid?
  end

  test 'should not create user without email' do
    user = User.new
    user.password = "1234qwerty"

    user.save
    refute user.valid?
  end

  test 'should not create user without password' do
    user = User.new
    user.email = "test@test.com"

    user.save
    refute user.valid?
  end

  test 'should not create user with wrong email formating' do
    user = User.new
    user.email = "test"
    user.password = "1234qwerty"

    user.save
    refute user.valid?
  end

  test 'should not create users with same emails' do
    user1 = User.new
    user1.email = "test@test.com"
    user1.password = "1234qwerty"

    user1.save
    assert user1.valid?

    user2 = User.new
    user2.email = "test@test.com"
    user2.password = "1234qwerty"

    user2.save
    refute user2.valid?
  end
end
