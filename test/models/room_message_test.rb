require 'test_helper'

class RoomMessageTest < ActiveSupport::TestCase
  test "should save valid message" do
    ms = RoomMessage.new
    ms.message = "Hello World!"
    ms.room_id = 1
    ms.user_id = 1

    ms.save
    assert ms.valid?
  end
  #test " should not save ms without a message" do #no null restraints applied to message
  # ms = RoomMessage.new
  # ms.room_id = 1
  # ms.user_id = 1

  # ms.save
  # refute ms.valid?
  #end
  #
  test " should not save ms without a room id" do
   ms = RoomMessage.new
    ms.message = "Hello World!"

   ms.user_id = 1

   ms.save
   refute ms.valid?
  end

  test " should not save ms without a user id" do
    ms = RoomMessage.new
    ms.message = "Hello World!"
    ms.room_id = 1


    ms.save
    refute ms.valid?
  end
end
