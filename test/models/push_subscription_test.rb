require 'test_helper'

class PushSubscriptionTest < ActiveSupport::TestCase
  test 'should save valid push' do
    ps = PushSubscription.new
    ps.endpoint = "www.google.co.uk"
    ps.user_id = 1


    ps.save
    assert ps.valid?
  end
  # test 'should not save push without endpoint' do #test fails because of null restraint even though its to refute
  # ps = PushSubscription.new
  # ps.user_id = 1


  # ps.save
  # refute ps.valid?
  #end

  test 'should not save push without user id' do
    ps = PushSubscription.new
    ps.endpoint = "www.google.co.uk"


    ps.save
    refute ps.valid?
  end
end
