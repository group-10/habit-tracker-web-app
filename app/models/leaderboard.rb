class Leaderboard < ApplicationRecord
  has_many :board_users, dependent: :destroy
  has_many :users, through: :board_users

  has_one :room, inverse_of: :leaderboard

  validates :title, presence: true
  validates :description, presence: true

  validates :title, length: { in: 4..20 }
  validates :title, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }\

  validates :description, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }
end
