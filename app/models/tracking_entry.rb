class TrackingEntry < ApplicationRecord
  include ActiveModel::Validations
  belongs_to :habit

  validates :habit_id, presence: true
  validates :trackStart, presence: true
  validates :trackEnd, presence: true
  validate :DurationViable?

  def DurationViable?
    return if [trackStart.blank?, trackEnd.blank?].any?
    if trackStart > trackEnd or trackStart == trackEnd
      errors.add(:trackStart, 'Duration must be positive')
    end
  end
end
