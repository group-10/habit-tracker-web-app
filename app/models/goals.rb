class Goals < ApplicationRecord
  belongs_to  :user

  validates :user_id, presence: true
  validates :title, presence: true
  validates :description, presence: true
  validates :counter, presence: true
  validates :target, presence: true
  validates :goaltype, presence: true

  validates :title, length: { in: 4..20 }
  validates :title, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }\

  validates :description, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }

  validates :counter, numericality: { greater_than: -1 }
  validates :target, numericality: {greater_than: -1}

  validates :goaltype, inclusion: { in: %w(habitAmount habitStreak ) }
end
