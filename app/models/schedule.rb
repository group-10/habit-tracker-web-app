class Schedule < ApplicationRecord
  belongs_to :habit

  validates :habit_id, presence: true
  validates :date, presence: true
  validates :starts_at, presence: true
  validates :ends_at, presence: true
  validate :DurationViable?

  def DurationViable?
    return if [starts_at.blank?, ends_at.blank?].any?
    if starts_at > ends_at
      errors.add(:starts_at, 'must be before end time')
    end
  end
end
