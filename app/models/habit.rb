class Habit < ApplicationRecord
  before_save :default_values
  belongs_to  :user
  has_many :schedules, dependent: :destroy

  validates :user_id, presence: true
  validates :title, presence: true
  validates :description, presence: true

  validates :title, length: { in: 4..20 }
  validates :title, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }\

  validates :description, format: { with: /\A[\s0-9a-zA-Z]+\z/,
    message: "Only allows letters, integers and whitespaces" }

  validates :streak, numericality: { greater_than: -1 }
  validates :target, numericality: { greater_than: 4 }

  # set target to 60 by default
  def default_values
    self.target = 60 if self.target.nil?
    self.last_tracked = DateTime.now
  end
end
