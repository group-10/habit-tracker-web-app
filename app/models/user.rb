class User < ApplicationRecord
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable,
         omniauth_providers: %i[facebook google_oauth2]

  def self.grab_profile_pic(token)
    access_token = token
    facebook = Koala::Facebook::API.new(access_token)
    facebook.get_object("me?fields=picture")['picture']['data']['url'].to_s
  end

  def self.get_friends(token)
    access_token = token
    @graph = Koala::Facebook::API.new(access_token)
    @graph.get_object("me?fields=friends")['friends']['data']
  end
  def self.from_omniauth(auth)
    priorAuth = Auth.find_by(provider: auth.provider, uid: auth.uid)
    if priorAuth
      return priorAuth.user
    end
    email = auth['info']['email']
    existing_user = find_for_database_authentication(email: email.downcase)
    if existing_user
      existing_user.add_oauth_authorization(auth).save
      return existing_user
    end
    create_new_user_from_oauth(auth, email)
  end
  #Remove tokens from users and auth
  #Store token in session var: fb_token, gg_token
  def add_oauth_authorization(data)
    Auth.new({provider: data['provider'],uid: data['uid'],user: User.find_by(email: data['info']['email'])})
  end

  def self.create_new_user_from_oauth(auth, email)
    user = User.new({
                      email: email,
                      name: auth.info.name,
                      password: Devise.friendly_token[0,20]
                    })
    if %w(facebook).include?(auth.provider)
      user.image= User.grab_profile_pic(auth['credentials']['token'])
    end
    if %w(google).include?(auth.provider)
      user.skip_confirmation!
    end
    user.add_oauth_authorization(auth)
    user.save
    user
  end
  has_many  :habits, dependent: :destroy
  has_many  :goals
  has_many  :push_subscriptions, dependent: :destroy
  has_many  :auths

  has_many :board_users, dependent: :destroy
  has_many :leaderboards, through: :board_users
end
