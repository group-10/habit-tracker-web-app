class GoalsController < ApplicationController
  # GET /goals/1 or /goals/1.json
  def show
  end

  # GET /goals/new
  def new
    @goal = Goal.new
  end

  # GET /goals/1/edit
  def edit
  end

  def create
    @goal = Goal.new(goal_params)
    @goal.user_id = current_user.id

  end

  def index
    helpers.updateAllGoals()
    helpers.generateHabitAmountGoals()
    helpers.generateHabitStreakGoals()
    helpers.generateMonthlyGoals()
    @goals = Goals.where(user_id: current_user.id, completed: "false")
  end

  def completed
    @goals = Goals.where(user_id: current_user.id, completed: "true")
  end

  # PATCH/PUT /goals/1 or /goals/1.json
  def update
    respond_to do |format|
      if @goal.update(goal_params)
        format.html { redirect_to @goal, notice: "Goal was successfully updated." }
        format.json { render :show, status: :ok, location: @goal }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1 or /goals/1.json
  def destroy
    @goal = Goals.find(params[:id])
    @goal.destroy
    respond_to do |format|
      format.html { redirect_to :goals, notice: "Goal was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goal
      @goal = Goals.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def goal_params
      params.require(:goal).permit(:title, :description, :counter, :target, :user_id, :goaltype, :completed)
    end
end
