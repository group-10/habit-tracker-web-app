class InvitesController < ApplicationController
  # POST /inviteUser
  def invite
    #Invite a user to leaderboard given name
    unless params[:name].nil? || params[:leaderboard].nil?
      @targetUsr = User.find_by_name(params[:name])
      @targetLdb = Leaderboard.find_by_id(params[:leaderboard])
      unless @targetUsr.nil? || @targetLdb.nil?
        if @targetUsr.board_users.find_by_leaderboard_id(@targetLdb.id).nil?
          invite = @targetUsr.board_users.new(user: @targetUsr, leaderboard: @targetLdb,status:"pending")
          if invite.save!
            flash.notice ="Invite was successfully created."
            redirect_back(fallback_location: leaderboards_path)
          end
        else
          flash.notice ="User already invited."
          redirect_back(fallback_location: leaderboards_path)
        end
      end
    end
  end

  # POST /acceptInvite
  def accept
    #change status to accepted
    unless params[:id].nil?
      @targetInv = BoardUser.find(params[:id])
      unless @targetInv.nil?
        @targetInv.status="accepted"
        if @targetInv.save!
          flash.notice ="Invite was successfully accepted."
          redirect_back(fallback_location: leaderboards_path)
        end
      end
    end
  end

  # POST /rejectInvite
  def reject
    #remove entry from table
    unless params[:id].nil?
      @targetInv = BoardUser.find(params[:id])
      unless @targetInv.nil?
        if @targetInv.destroy!
          flash.notice ="Invite was successfully rejected."
          redirect_back(fallback_location: leaderboards_path)
        end
      end
    end
  end

  # POST /leaveBoard
  def leave
    #leave a leaderboard
    unless params[:id].nil?
      @targetEnt = BoardUser.where(leaderboard_id: params[:id], user_id: current_user.id)
      unless @targetEnt.nil?
        if @targetEnt.first.destroy!
          flash.notice ="Successfully left the leaderboard"
          redirect_to(leaderboards_path)
        end
      end
    end
  end
end
