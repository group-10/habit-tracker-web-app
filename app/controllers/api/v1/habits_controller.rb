require 'webpush'

module Api
  module V1
    class HabitsController < ApplicationController
      skip_before_action :verify_authenticity_token

      def index
        unless current_user.nil?
          habits = Habit.find_by_user_id(current_user)
        else
          habits = Habit.find_by_user_id(params[:user_id])
        end
        render json: HabitSerializer.new(habits).serialized_json
      end


      def show
        habit = Habit.find_by(id: params[:id])
        render json: HabitSerializer.new(habit).serialized_json
      end

      # POST api/v1/habits/add_push_endpoint
      # gets endpoint it and stores it in database
      # so that users can get notifications specific to them on all their devices
      # (by default subscription is related to specific browser and not user
      # so one user can have multiple subscriptions)
      def add_push_endpoint
        if PushSubscription.where(endpoint: params[:subscription][:endpoint]).empty?
          @subscription = PushSubscription.new(
            endpoint: params[:subscription][:endpoint],
            p256dh: params[:subscription][:keys][:p256dh],
            auth: params[:subscription][:keys][:auth])
          @subscription.user = current_user
          @subscription.save
        end
      end

      # POST api/v1/habits/push_notification
      # method for pushing a notifications,
      # loops through all user subscriptions and sends a payload with their email as message
      # if subscription is outdated or otherwose Invalid it is destroyed
      def push_notification
        current_user.push_subscriptions.each do |subscription| begin
          Webpush.payload_send(
            message: subscription.user.email,
            endpoint: subscription.endpoint,
            p256dh: subscription.p256dh,
            auth: subscription.auth,
            vapid: {
              subject: "mailto:sender@example.com",
              public_key: ENV['VAPID_PUBLIC_KEY'],
              private_key: ENV['VAPID_PRIVATE_KEY'],
              expiration: 60*59*24
            },
            ssl_timeout: 5, # value for Net::HTTP#ssl_timeout=, optional
            open_timeout: 5, # value for Net::HTTP#open_timeout=, optional
            read_timeout: 5 # value for Net::HTTP#read_timeout=, optional
          )
          rescue Webpush::InvalidSubscription => exception
            subscription.destroy
          rescue Webpush::ExpiredSubscription => exception
            subscription.destroy
          end
        end
      end
    end
  end
end
