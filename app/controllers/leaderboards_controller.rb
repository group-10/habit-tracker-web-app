class LeaderboardsController < ApplicationController
  before_action :set_leaderboard, only: %i[ show edit update destroy ]
  before_action :user_check, only: %i[ show edit update destroy ]

  # GET /leaderboards
  def index
    @leaderboards = Leaderboard.where(board_users: BoardUser.where(user: current_user, status: "accepted"))
  end

  # GET /leaderboards/1/
  def show
    @leaderboard = Leaderboard.find(params[:id])
    @room = Room.find(@leaderboard.room_id)
  end

  # GET /leaderboards/new/
  def new
    @leaderboard = Leaderboard.new
  end

  # GET /leaderboards/1/edit
  def edit
  end

  # POST /leaderboards
  def create
    @leaderboard = Leaderboard.new(leaderboard_params)
    @room = Room.new(leaderboard: @leaderboard)
    @room.save

    @leaderboard.board_users.new(user: current_user, status:"accepted")
    respond_to do |format|
      if @leaderboard.save
        format.html { redirect_to @leaderboard, notice: "Leaderboard was successfully created." }
        format.json { render :show, status: :created, location: @leaderboard }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @leaderboard.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leaderboards/1 or /leaderboards/1.json
  def update
    respond_to do |format|
      if @leaderboard.update(leaderboard_params)
        format.html { redirect_to @leaderboard, notice: "Leaderboard was successfully updated." }
        format.json { render :show, status: :ok, location: @leaderboard }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @leaderboard.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @leaderboard = Leaderboard.find(params[:id])
    @leaderboard.destroy
    respond_to do |format|
      format.html { redirect_to leaderboards_url, notice: "Leaderboard was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # POST /joinWcode
  def join
    unless params[:code].nil?
      code = params[:code]
      parts = code.split('|')
      lbID = parts.first
      usrID = parts.last

      @tgtLb = Leaderboard.find(lbID)
      unless @tgtLb.nil?
        #If the leaderboard exists and the usrID exists on the leaderboard
        #add current user to the leaderboard
        unless @tgtLb.users.find(usrID).nil?
          entry = current_user.board_users.new(user: current_user, leaderboard: @tgtLb,status:"accepted")
          if entry.save!
            flash.notice ="Leaderboard was successfully joined."
            redirect_back(fallback_location: leaderboards_path)
          end
        end
      end
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leaderboard
      if Leaderboard.exists?(params[:id])
      @leaderboard = Leaderboard.find(params[:id])
      else
        redirect_to leaderboards_url, notice: "Sorry, that leaderboard does not exist."
        end
    end

    # Only allow a list of trusted parameters through.
    def leaderboard_params
      params.require(:leaderboard).permit(:title, :description)
    end
  def user_check
    if !(@leaderboard.user_ids.include?(current_user.id))
      redirect_to leaderboards_url, notice: "Sorry, but you are only allowed to view the leaderboards you are currently in."
    end
  end
end
