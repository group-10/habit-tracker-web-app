class LegalController < ApplicationController
  #  GET policy/privacy
  def privacy
  end

  #  GET policy/security
  def security
  end

  #  GET policy/cookie-notice
  def cookieInfo
  end
end
