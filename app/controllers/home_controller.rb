class HomeController < ApplicationController
  before_action :authenticate_user!

  # GET *
  def home
    @habits = Habit.find_by_user_id(current_user.id)

    @decodedVapidPublicKey = Base64.urlsafe_decode64(ENV['VAPID_PUBLIC_KEY']).bytes
  end
end
