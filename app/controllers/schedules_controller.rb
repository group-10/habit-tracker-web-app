class SchedulesController < ApplicationController
  before_action :set_schedule, only: %i[ show edit update destroy ]
  before_action :get_habit
  # GET /schedules or /schedules.json
  def index
    @schedules = @habit.schedules
  end

  # GET /schedules/1 or /schedules/1.json
  def show
  end

  # GET /schedules/new
  def new
    @schedule = @habit.schedules.build
      #unless current_user.auths.where(provider: "google_oauth2").empty?
      #cal = Google::Calendar.new(:client_id     => ENV[:GOOGLE_CLIENT_ID],
      #                           :client_secret => ENV[:GOOGLE_CLIENT_SECRET],
      #                           :calendar      => "default",
      #                           :redirect_url  => "urn:ietf:wg:oauth:2.0:oob" # this is what Google uses for 'applications'
      #)
      #cal.login_with_refresh_token(session[:gg_token])
      #@events = cal.events
      #    end
  end

  # GET /schedules/1/edit
  def edit
  end

  # POST /schedules or /schedules.json
  def create
    @schedule = @habit.schedules.build(schedule_params)
    @schedule.habit_id = @habit.id
    respond_to do |format|
      if @schedule.save
        format.html { redirect_to habit_schedules_path(@habit), notice: "Schedule was successfully created." }
        format.json { render :show, status: :created, location: @schedule }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedules/1 or /schedules/1.json
  def update
    respond_to do |format|
      if @schedule.update(schedule_params)
        format.html { redirect_to habit_schedules_path(@habit), notice: "Schedule was successfully updated." }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1 or /schedules/1.json
  def destroy
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to habit_schedules_path(@habit), notice: "Schedule was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    def get_habit
      @habit = Habit.find(params[:habit_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @habit = Habit.find(params[:habit_id])
      @schedule = @habit.schedules.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def schedule_params
      params.require(:schedule).permit(:habit_id, :date, :starts_at, :ends_at)
    end
end
