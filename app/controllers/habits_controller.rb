class HabitsController < ApplicationController
  before_action :set_habit, only: [:edit, :update, :destroy, :track]
  before_action :user_check, only:[:edit, :update, :destroy, :track]

  # GET /habits/new
  def new
    @habit = Habit.new
  end

  # GET /habits/1/edit
  def edit
  end

  # POST /habits or /habits.json
  def create
    @habit = Habit.new(habit_params)
    @habit.user_id = current_user.id
    @habit.streak = 0
    @habit.max_streak = 0
    respond_to do |format|
      if @habit.save
        format.html { redirect_to :root, notice: "Habit was successfully created." }
        format.json { render :show, status: :created, location: 'home#home' }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @habit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /habits/1 or /habits/1.json
  def update
    respond_to do |format|
      if @habit.update(habit_params)
        format.html { redirect_to :root, notice: "Habit was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @habit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /habits/1 or /habits/1.json
  def destroy
    @habit.destroy
    respond_to do |format|
      format.html { redirect_to :root, notice: "Habit was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # POST hasbits/1/track
  # Increases streak for specific habit, or resets it if the streak was lost
  def track
    if @habit.last_tracked.nil? || @habit.last_tracked.to_date == Date.yesterday
      @habit.streak = @habit.streak + 1
      @habit.last_tracked = DateTime.now
      @habit.save

      respond_to do |format|
        format.html { redirect_to :root, notice: 'Good job!'  }
        format.js {}
      end
    elsif @habit.last_tracked.to_date < Date.yesterday
      @habit.streak = 1
      @habit.last_tracked = DateTime.now
      @habit.save

      respond_to do |format|
        format.html { redirect_to :root, alert: "You've lost you streak. Too bad!"  }
        format.js {}
      end
    else
      respond_to do |format|
        format.html { redirect_to :root, notice: 'You have already done that today. Come back tomorrow!'  }
      end
    end

    if @habit.max_streak.nil?
      @habit.max_streak = @habit.streak
      @habit.save
    elsif @habit.streak > @habit.max_streak

      @habit.max_streak = @habit.streak
      @habit.save
    end
  end

  def reset_streak
    @habit = Habit.find(params[:id])
    @habit.streak = 0
    @habit.save
  end

  def reset_max_streak
    @habit = Habit.find(params[:id])
    @habit.max_streak = @habit.streak
    @habit.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_habit
      if Habit.exists?(params[:id])
      @habit = Habit.find(params[:id])
      else
        redirect_to root_path, notice: "Sorry, that habit does not exist."
      end
    end

    # Only allow a list of trusted parameters through.
    def habit_params
      params.require(:habit).permit(:title, :description, :target)
    end

    def user_check
      if current_user.id != @habit.user_id
        redirect_to root_path, notice: "Sorry, but you are only allowed to view your own habits."
      end
    end
end
