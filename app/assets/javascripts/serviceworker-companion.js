if (!("Notification" in window)) {
  console.error("This browser does not support desktop notification");
}

// Check whether notification permissions have already been granted
else if (Notification.permission === "granted") {
  console.log("Permission to receive notifications has been granted");
}

// Otherwise, asks the user for permission
else if (Notification.permission !== 'denied') {
  Notification.requestPermission(function (permission) {
  // If the user accepts, creates a notification
    if (permission === "granted") {
      console.log("Permission to receive notifications has been granted");
    }
  });
}

// registers a serviceworker
if (navigator.serviceWorker) {
  navigator.serviceWorker.register('/serviceworker.js', { scope: './' })
  .then(function(reg) {
    console.log('Service worker change, registered the service worker');
  });

  // when the serviceworker is ready subscription is created
  // and posted to backend so it can be stored in the db
  navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
    serviceWorkerRegistration.pushManager
    .subscribe({
      userVisibleOnly: true,
      applicationServerKey: window.vapidPublicKey
    }).then(function() {
        navigator.serviceWorker.ready
        .then((serviceWorkerRegistration) => {
          serviceWorkerRegistration.pushManager.getSubscription()
          .then((subscription) => {
            $.post("/api/v1/habits/add_push_endpoint", { subscription: subscription.toJSON() });
          });
        });
      });
    });
}
// Otherwise, no push notifications :(
else {
  console.error('Service worker is not supported in this browser');
}
