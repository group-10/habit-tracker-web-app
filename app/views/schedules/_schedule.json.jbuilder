json.extract! schedule, :id, :habit_id, :start, :end, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
