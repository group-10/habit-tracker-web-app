class HabitSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :title, :streak, :target
end
