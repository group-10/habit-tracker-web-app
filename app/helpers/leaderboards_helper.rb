module LeaderboardsHelper
  # Helper methods for leaderboards

  # Get user score based on sum of all their streaks
  def calc_score(user, leaderboard)
    temp = leaderboard.board_users.find_by(user: user)
    @new_score = 0
    @old_score = temp.score
    user.habits.each_with_index do |habit, i|
      @new_score = @new_score + habit.streak
    end
    if @new_score != @old_score
      temp.score = @new_score
      temp.save!
    end
  end

  # Get user by id?
  def get_user(userid)
    @user = User.find_by_id(userid)
  end

  # Sort user in a leaderboard
  def sort_users(leaderboard)
    @users = User.where(board_users: BoardUser.where(leaderboard: leaderboard, status: "accepted"))
    @users.each{|u| calc_score(u,leaderboard)}
    @users.sort_by{|u| leaderboard.board_users.find_by(user: u).score}.reverse!
  end

  # Returns all users in a leaderboard
  def get_all_users(leaderboard)
    @users = User.all
    @leaderboard.user_ids = []
    @users.each do |user|
      @leaderboard.user_ids = @leaderboard.user_ids<< user.id
    end
  end
end
