module HabitsHelper
  # Helper method that updates a habit streak and make sure it is mantained
  # Requires habit as an argument
  def get_habit_streak(habit)
    if habit.last_tracked.to_date < Date.yesterday
      habit.streak = 1
      habit.last_tracked = DateTime.now
      habit.save

      unless Rails.env.test?
        flash.now[:alert] = "You've lost you streak. Too bad!"
      end
      return habit.streak
    else
      puts habit.streak
      return habit.streak
    end
  end
end
