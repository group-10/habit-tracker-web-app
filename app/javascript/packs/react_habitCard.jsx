import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import axios from 'axios';
import '@fontsource/roboto';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core/styles';
const ProgressTheme = createMuiTheme({
    palette: {
        secondary: {
            main: '#f50057'
        },
        text: {
            secondary: '#ffffff'
        }
    },
});
class Habit{
    constructor(json) {
        this.Id = json.id;
        this.Streak = json.streak;
        this.Title = json.title;
        this.Target = json.target;
    }
}
function CircProgWLabel(value, label) {
    return (
        <ThemeProvider theme={ProgressTheme}>
            <Box style={{ position: 'relative', display: 'inline-flex' }} height={'auto'} width={'auto'}>
                <CircularProgress variant="determinate" value={value} size={100} color="secondary"/>
                <Box style={{ top: 0, left: 0, bottom: 0, right: 0, position: 'absolute', display: 'flex', alignItems: 'center', justifyContent: 'center' }}			>
                    <Typography variant="h4" component="div" color="textSecondary">{label}</Typography>
                </Box>
            </Box>
        </ThemeProvider>
    );
}
function UpdateProgressWheels(){
    let HabitCards = document.getElementsByClassName("HabitCard")
    for (let item of HabitCards) {
        let habit;
        axios.get("/api/v1/habits/show?id="+item.id)
            .then(function (resp) {
                let APIjson = resp.data.data.attributes;
                habit = new Habit(APIjson);
                ReactDOM.render(
                    CircProgWLabel(habit.Streak / habit.Target * 100, habit.Streak),
                    document.getElementById(habit.Id).appendChild(document.createElement('div')),
                )
            })
            .catch(function(err){
                console.error(err)
            })
    }
}
//Update the progress wheels whenever the page is loaded or one of the habits is tracked
$(document).ready(function() {
    $("[id^=Track]").bind("click",UpdateProgressWheels());
})
