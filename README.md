# Group 10 Habit Tracker Web App
### How to run it?
#### Docker:
1. Make sure docker is installed
2. Clone the repository
3. cd into root repository
4. run `docker-compose build`
[![docker-compose build](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/c8801591118df1e7c99085cc35493817/image.png "docker-compose build")](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/c8801591118df1e7c99085cc35493817/image.png "docker-compose build")
5. run `docker-compose up -d`
[![docker-compose up](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/1b0c612ebe165e45fe7f0c071e55bfe3/image.png "docker-compose up")](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/1b0c612ebe165e45fe7f0c071e55bfe3/image.png "docker-compose up")
6. Run `docker container ls` to see and verify running containers.
[![docker container ls](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/6b3348acfee9d5908c1fb93a6279ba0c/image.png "docker container ls")](https://gitlab.eps.surrey.ac.uk/group-10/habit-tracker-web-app/-/wikis/uploads/6b3348acfee9d5908c1fb93a6279ba0c/image.png "docker container ls")
7.  Run `docker exec CONTAINER_ID rake db:create`,  `docker exec CONTAINER_ID rake db:migrate`  and `docker exec CONTAINER_ID rake db:seed` to setup the database, where CONTAINER_ID is container id of habbittrackerwebappmaster_web (or any other name of web image) image.
8. Access the website on  localhost:3000 or 127.0.0.1:3000.
9. For some machines (especially when using Windows) site might be running on another ip, to find it run `docker-machine ip default`

NOTE: This process was tested on Docker for Windows and might change depending on your platform.
#### Alternatively (rails server):
 Run as normal rails application with `bundle install` and `rails server`
#### Alternatively (visit deployed version):
[Use this link to access our site on Heroku](hthttps://group-ten-habits-deployment.herokuapp.com/#_=_tp:// "Use this link to access our site on Heroku")
